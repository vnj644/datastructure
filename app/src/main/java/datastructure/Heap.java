package datastructure;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
// method "add" need обобщение
public class Heap <T extends BinaryHeap> implements Collection <T> {

	private T[] heapArray;
	private int maxSize;
	private int currentSize;

	public Heap(int maxSize) {
		this.maxSize = maxSize;
		this.currentSize = 0;
		heapArray = (T[]) new BinaryHeap[maxSize];
	}

	public T[] toArray() {
		return heapArray;
	}

	public T[] toArray(Object[] c) {
		return heapArray;
	}

	public Iterator<T> iterator() {
		return new Iterator<T>() {
			private int nextIndex = 0;

			public boolean hasNext() {
				return (nextIndex <= currentSize - 1);
			}

			public T next() {
				T retValue = heapArray[nextIndex];
				nextIndex += 1;
				return retValue;
			}

		};
	}

	public boolean add(T newNode) {
		int maxSizeConst = 100;
		if (currentSize == maxSize) {
			heapArray = Arrays.copyOf(heapArray, currentSize + 1);
			maxSize+=maxSizeConst;
		}
		heapArray[currentSize] = (T) newNode;
		displaceUp(currentSize++);// сортировка (переносим вершину вверх)
		return true;
	}

	private void displaceUp(int index) {
		int parentIndex = (index - 1) / 2;
		T bottom = heapArray[index];
		while (index > 0 && heapArray[parentIndex].getValue() > bottom.getValue()) {
			heapArray[index] = heapArray[parentIndex];
			index = parentIndex;
			parentIndex = (parentIndex - 1) / 2;
		}
		heapArray[index] = bottom;
	}

	public boolean remove(Object element) {
		if (contains(element)) {
			int index = 0;
			int i = 0;
			while (element != heapArray[i]) {
				i++;
			}
			index = i;
			T root = heapArray[index];
			heapArray[index] = heapArray[--currentSize];
			heapArray[currentSize] = null;
			displaceDown(index);
			return true;
		}
		return false;
	}

	public T pop() {
		T root = heapArray[0];
		heapArray[0] = heapArray[--currentSize];
		heapArray[currentSize] = null;
		displaceDown(0);
		return root;
	}

	public T peek() {
		return heapArray[0];
	}

	private void displaceDown(int index) {
		int largerChild;
		T top = heapArray[index];
		while (index < currentSize / 2) {
			int leftChild = 2 * index + 1;
			int rightChild = leftChild + 1;

			if (rightChild > currentSize && heapArray[leftChild].getValue() > heapArray[rightChild].getValue()) {
				largerChild = rightChild;
			} else {
				largerChild = leftChild;
			}

			if (top.getValue() <= heapArray[largerChild].getValue()) {
				break;
			}

			heapArray[index] = heapArray[largerChild];
			index = largerChild;
		}
		heapArray[index] = top;
	}

	public boolean contains(Object element) {
		for (int i = 0; i < currentSize; i++) {
			if (element == heapArray[i]) {
				return true;
			}
		}
		return false;
	}

	public void clear() {
		for (int i = 0; i < currentSize; i++) {
			heapArray[i] = null;
		}
	}

	public int size() {
		return (heapArray[0] == null) ? 0 : currentSize;
	}

	public boolean retainAll(Collection<?> c) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	public boolean removeAll(Collection<?> c) {
		BinaryHeap[] save = Arrays.copyOf(heapArray, heapArray.length);
		for (Object element : c) {
			this.remove(element);
		}
		if (Arrays.equals(save, heapArray)) {
			return false;
		} else {
			return true;
		}
	}

	public boolean addAll(Collection <? extends T> c) {
		if (currentSize + c.size() >= maxSize) {
			maxSize = currentSize + c.size();
			heapArray = Arrays.copyOf(heapArray, maxSize);
		}
		for (Object element : c) {
			heapArray[currentSize++] = (T) element;
		}
		displaceUp(maxSize - 1);// сортировка (переносим вершину вверх)
		return true;
	}

	public boolean containsAll(Collection<?> c) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	public boolean isEmpty() {
		return (heapArray[0] == null);
	}

	public int hashCode(T e) {
		int res = 12;
		res = 12 * res + (e != null ? e.hashCode() : 0);
		return res;
	}
	public boolean equals(BinaryHeap element1, BinaryHeap element2) {
		if (element1 == element2)
			return true;
		if (element1 == null || element2.getClass() != element1.getClass())
			return false;
		return element1.getValue() == element2.getValue();
	}

	public String toString() {
		String s = "";
		if (heapArray[0] == null) {
			return "binary heap is empty";
		}
		for (int i = 0; i < currentSize - 1; i++) {
			s = s + String.valueOf(heapArray[i].getValue()) + ", ";
		}
		s += String.valueOf(heapArray[currentSize - 1].getValue());
		return s;
	}
}
